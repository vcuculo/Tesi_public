function [X,Y,Z] = ThreeDCoordFromVideoWeb(vidPath,intrinsicsOfCamera)
       
    focalX=intrinsicsOfCamera.FocalLength(1);
    focalY=intrinsicsOfCamera.FocalLength(2);
    centerX=intrinsicsOfCamera.PrincipalPoint(1);
    centerY=intrinsicsOfCamera.PrincipalPoint(2);
    extract=true;
    v = VideoReader(vidPath);
    if(isunix)
        executable = '"./libraries/OF/FeatureExtraction"';
    else
        executable = '"./libraries/OpenFace_win_bin/bin/FeatureExtraction.exe"';
    end
    output = './output_3Dfeatures_vid/';
    
   

    if(~exist(output, 'dir'))
        mkdir(output)   
    else
        extract=false;
    end
    
    in_files = dir(vidPath);
    

    command = executable;

    % Remove for a speedup
    command = cat(2, command, ' -verbose ');

    % add all videos to single argument list (so as not to load the model anew
    % for every video)
    for i=1:numel(in_files)
        inputFile = ['./videos/', in_files(i).name];
        [~, name, ~] = fileparts(inputFile);
    
        % where to output tracking results
        outputFile = [output name '.txt'];
            
        if(~exist( outputFile , 'file'))
            edit(outputFile);
        end
        command = cat(2, command, [' -f "' inputFile '" -of "' outputFile '" -fx ' focalX '" -fy ' focalY ' -cx ' centerX ' -cy ' centerY '-no2dFp']);           
    end

    if(extract)
        if(isunix)
            %unix(command);
        else
            dos(command);
        end  
    end
    
    N = v.NumberOfFrames;
    %primacolonna X=151 Y=218=151+67 Z=
    firstCol=152;
    X=csvread(outputFile ,1,firstCol,[1 firstCol N firstCol+67]);
    firstCol=firstCol+68;
    Y=csvread(outputFile ,1,firstCol,[1 firstCol N firstCol+67]);
    firstCol=firstCol+68;
    Z=csvread(outputFile ,1,firstCol,[1 firstCol N firstCol+67]);
    
    fclose('all');
    

end

