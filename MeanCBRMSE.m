function [Mrmse3D,Mrmse2D,RMSEs2,RMSEs3] = MeanCBRMSE(Xo, Yo, Zo, kinectCoordsDir)
    %Calculation of RMSE for the 3D coordinates 
    Xk=[];
    Yk=[];
    Zk=[];
    Mrmse3D=0;
    Mrmse2D=0;
    
    %centering OpenFace coordinates in Outer upper edge of left upper eyelid
    irisL=[Xo(:,41) Yo(:,41) Zo(:,41)];
    for frameIndex=1:size(Xo,1)
     for Ofindex=1:size(Xo,2)
        Xc(frameIndex,Ofindex)=(Xo(frameIndex,Ofindex)-irisL(frameIndex,1));
        Yc(frameIndex,Ofindex)=(Yo(frameIndex,Ofindex)-irisL(frameIndex,2))*(-1);
        Zc(frameIndex,Ofindex)=(Zo(frameIndex,Ofindex)-irisL(frameIndex,3));
     end
    end
    
    kinectFiles=dir(kinectCoordsDir);
    files_count=0;
    for k=1:length(kinectFiles)
        completePath=strcat(kinectCoordsDir,'/',kinectFiles(k).name);
        fileID = fopen(completePath);
        
        if(fileID~=-1)
          [Xfk,Yfk,Zfk,nCandide,dataKin]=mapKinectToCandide(fileID);
          dataKins(:,:,k)=dataKin;
          Xk=[Xk ; Xfk];
          Yk=[Yk ; Yfk];
          Zk=[Zk ; Zfk];
          files_count=files_count+1;
          fclose(fileID);
        end
       
    end
    X=Xc;
    Y=Yc;
    Z=Zc;
    candideIndexes=[6,7,8,9,11,16,17,18,21,24,27,29,30,31,32,33,34,41,49,50,51,54,57,60,62,63,64,65,66,67,80,81,82,83,84,85,86,87,88,89,90,95,98,99,100,101,106,107,108,109,112,113];
    candideKinIndexes=[36, 1, 54,53,58,57,24,20, 21, 25, 95, 94, 93];
    countPoints=0;
    nCandideKinectPoints=length(candideKinIndexes);
    for i=1:nCandideKinectPoints
        if(find(candideIndexes==candideKinIndexes(i)))
            countPoints=countPoints+1;
        end
    end
    
    nFrames= files_count;
    for frameIndex = 1 : nFrames
        rmse3D=0;
        rmse2D=0;
        divX=0;
        divY=0;
        divZ=0;
        for candIndex = 1 : nCandideKinectPoints 
            cind=find(candideIndexes==candideKinIndexes(candIndex));
            if(cind)
              %centering kinect coordinates in Outer upper edge of left upper eyelid  
              Xcf=Xk(frameIndex,cind)-Xk(frameIndex,95);
              Ycf=Yk(frameIndex,cind)-Yk(frameIndex,95);
              Zcf=Zk(frameIndex,cind)-Zk(frameIndex,95);
              divX=((X(frameIndex,cind)/1000)-Xcf)^2;
              divY=((Y(frameIndex,cind)/1000)-Ycf)^2;
              divZ=((Z(frameIndex,cind)/1000)-Zcf)^2;
              rmse3D=rmse3D+divX+divY+divZ;
              rmse2D=rmse2D+divX+divY; 
            end
        end
        RMSEs2(frameIndex)=sqrt(rmse2D/countPoints);
        RMSEs3(frameIndex)=sqrt(rmse3D/countPoints);
        Mrmse3D=Mrmse3D+sqrt(rmse3D/countPoints);
        Mrmse2D=Mrmse2D+sqrt(rmse2D/countPoints);      
    end
    
    Mrmse3D=Mrmse3D/nFrames;
    Mrmse2D=Mrmse2D/nFrames;

end
