%Written by Alessandra Saitta(based on Vittorio Cuculo's Snippets)
function [Rs,Ts,IPts]=ExstrinscsFromVideo(vidPath,intrinsicsOfCamera)
    v = VideoReader(vidPath);
    count=1;
    if(isunix)
        executable = '"./libraries/OF/FeatureExtraction"';
    else
         executable = '"./libraries/OpenFace_win_bin/bin/FeatureExtraction.exe"';
    end
    output = './output_features_vid/';

    if(~exist(output, 'file'))
        mkdir(output)
    end
    
    in_files = dir(vidPath);
    % some parameters
    verbose = true;

    extract=true;
    command = executable;

    % Remove for a speedup
    command = cat(2, command, ' -verbose ');

    % add all videos to single argument list (so as not to load the model anew
    % for every video)
    for i=1:numel(in_files)
        inputFile = ['./videos/', in_files(i).name];
        [~, name, ~] = fileparts(inputFile);
    
        % where to output tracking results
        outputFile = [output name '.txt'];
            
        if(~exist(outputFile, 'file'))
            edit(outputFile);
        else
            extract=false;
        end
        command = cat(2, command, [' -f "' inputFile '" -of "' outputFile '"']);        
        %command = cat(2, command, [' -simalign "' outputDir_aligned '" -hogalign "' outputHOG_aligned '"' ]);    
                 
    end

    if (extract)
  	 if(isunix)
        %unix(command);
     else
            dos(command);
     end
    end
    N = v.NumberOfFrames;
    fileID = fopen(outputFile);
    while count<N
        %annotations = textscan(fileID, '%f', 'HeaderLines', 1, 'Delimiter',',');
        %annotations = reshape(annotations{1},[],N)';
        %ofMatrix = [annotations(count,17:84)' annotations(count,85:152)'];
        
        ofMatrix= csvread(outputFile ,count,16,[count 16 count 151]);
        ofMatrix=reshape(ofMatrix,[],2);
        [R,T,IP]=FaceLandmarkandPosit(ofMatrix,intrinsicsOfCamera);
        Rs(:,:,count)=R;
        Ts(:,:,count)=T;
        IPts(:,:,count)=IP;
        count=count+1;
    end
    fclose('all');
end