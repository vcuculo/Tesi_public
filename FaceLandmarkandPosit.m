%Written by Alessandra Saitta(based on Vittorio Cuculo's Snippets)
%funzione per estrarre matrici di Rotazione partendo da Landmark Facciali estratte con OpenFace 
function [R,T,imagePts] = FaceLandmarkandPosit(ofMatrix,intrinsicsOfCamera)
    focalL=intrinsicsOfCamera.FocalLength(1);
    center=intrinsicsOfCamera.PrincipalPoint;  
    candideMatrix=importdata('./libraries/EditCandide/CandideLinearModel/Candide/Data/candide3.dat');
    candideIndex=[6,7,8,9,11,16,17,18,21,24,27,29,30,31,32,33,34,41,49,50,51,54,57,60,62,63,64,65,66,67,80,81,82,83,84,85,86,87,88,89,90,95,98,99,100,101,106,107,108,109,112,113];
    openFaceIndex=[31,34,52,58,9,18,20,22,37,40,32,3,1,6,49,8,51,67,27,25,23,46,43,36,15,17,12,55,10,53,50,54,62,64,68,66,60,56,63,61,65,29,38,45,42,47,39,44,41,48,33,35];
    imagePts=zeros(52,2);
    objectPts=zeros(52,3);
    
    countof=0;
    countc=0;
    
    for i= 1:68
        if(ismember(i,openFaceIndex))
            countof=countof+1;
            for j= 1:2
                
                imagePts(countof,j)= ofMatrix(i,j);
            end
        end
        
    end
    
    for i= 1:113
        if(ismember(i,candideIndex))
            countc=countc+1;
            for j= 1:3
                objectPts(countc,j)= candideMatrix(i,j);          
            end
        end
        
    end
    [R,T]=modernPosit(imagePts, objectPts, focalL, center);


end