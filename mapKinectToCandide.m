%Map to candide points feature points of a frame
function [Xfk,Yfk,Zfk,nCandide,dataKin] = mapKinectToCandide(fileID)
    % load Kinect points
    
    dataKin = fread(fileID, 'float'); 
    dataKin = reshape(dataKin,3,[])';
    
    %load candide Points
    candideMatrix=importdata('./libraries/EditCandide/CandideLinearModel/Candide/Data/candide3.dat');
    nCandide=size(candideMatrix,1);
    Xfk= zeros(nCandide,1);
    Yfk= zeros(nCandide,1);
    Zfk= zeros(nCandide,1);

    % Tracked points (fiducial)
    kinectIndexes=[29,30,470,242,1105,211,844,732,1118,1091,25,152,773];
    % corresponding CANDIDE-3 points
    candideIndexes=[36, 1, 54,53,58,57,24,20, 21, 25, 95, 94, 93];
    
    for i=1: 13
        tempC=candideIndexes(i);
        tempK=kinectIndexes(i);
        Xfk(tempC)=dataKin(tempK,1);
        Yfk(tempC)=dataKin(tempK,2);
        Zfk(tempC)=dataKin(tempK,3);
    end
    Xfk=Xfk';
    Yfk=Yfk';
    Zfk=Zfk';
end
