load cameraParamsKinect.mat
%estrazione parametri intrinseci webcam
% % Define images to process
% imageFileNames = {'./KinectCalibrationIMGs/ph6.jpg',...
%     './KinectCalibrationIMGs/ph8.jpg',...
%     './KinectCalibrationIMGs/ph9.jpg',...
%     './KinectCalibrationIMGs/ph10.jpg',...
%     './KinectCalibrationIMGs/ph14.jpg',...
%     './KinectCalibrationIMGs/ph16.jpg',...
%     './KinectCalibrationIMGs/ph17.jpg',...
%     './KinectCalibrationIMGs/ph22.jpg',...
%     './KinectCalibrationIMGs/ph23.jpg',...
%     './KinectCalibrationIMGs/ph24.jpg',...
%     './KinectCalibrationIMGs/ph26.jpg',...
%     './KinectCalibrationIMGs/ph27.jpg',...
%     './KinectCalibrationIMGs/ph28.jpg',...
%     './KinectCalibrationIMGs/ph29.jpg',...
%     './KinectCalibrationIMGs/ph31.jpg',...
%     './KinectCalibrationIMGs/ph32.jpg',...
%     './KinectCalibrationIMGs/ph36.jpg',...
%     };
% 
% 
% % Detect checkerboards in images
% [imagePoints, boardSize, imagesUsed] = detectCheckerboardPoints(imageFileNames);
% imageFileNames = imageFileNames(imagesUsed);
% 
% % Read the first image to obtain image size
% originalImage = imread(imageFileNames{1});
% [mrows, ncols, ~] = size(originalImage);
%   
% % Generate world coordinates of the corners of the squares
% squareSize = 18;  % in units of 'millimeters'
% worldPoints = generateCheckerboardPoints(boardSize, squareSize);
% 
% % Calibrate the camera
% [cameraParamsW, imagesUsedW, estimationErrorsW] = estimateCameraParameters(imagePoints, worldPoints, ...
%     'EstimateSkew', true, 'EstimateTangentialDistortion', false, ...
%     'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'millimeters', ...
%     'InitialIntrinsicMatrix', [], 'InitialRadialDistortion', [], ...
%     'ImageSize', [mrows, ncols]);

[X,Y,Z]=ThreeDCoordFromVideoWeb('./videos/10_2.mp4',cameraParamsW);
[mean3D,mean2D,RMSE2,RMSE3]=MeanOFRMSE(X,Y,Z,'./KinectDataSource/data_10_2');
disp("I'm OF");
disp(mean3D);
c=[(X(47,:))' (Y(47,:)*(-1))' Z(47,:)'];
plotmatrix(c);
std2D=std(RMSE2);
std3D=std(RMSE3);