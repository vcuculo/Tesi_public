clear
load cameraParamsKinect.mat

%risistemare MATRICI COME IN articolo e applicare ultimo  calcolo par 4
%articolo
%estrazione parametri intrinseci webcam
% Define images to process
% imageFileNames = {'.\KinectCalibrationIMGs\ph6.jpg',...
%     '.\KinectCalibrationIMGs\ph8.jpg',...
%     '.\KinectCalibrationIMGs\ph9.jpg',...
%     '.\KinectCalibrationIMGs\ph10.jpg',...
%     '.\KinectCalibrationIMGs\ph14.jpg',...
%     '.\KinectCalibrationIMGs\ph16.jpg',...
%     '.\KinectCalibrationIMGs\ph17.jpg',...
%     '.\KinectCalibrationIMGs\ph22.jpg',...
%     '.\KinectCalibrationIMGs\ph23.jpg',...
%     '.\KinectCalibrationIMGs\ph24.jpg',...
%     '.\KinectCalibrationIMGs\ph26.jpg',...
%     '.\KinectCalibrationIMGs\ph27.jpg',...
%     '.\KinectCalibrationIMGs\ph28.jpg',...
%     '.\KinectCalibrationIMGs\ph29.jpg',...
%     '.\KinectCalibrationIMGs\ph31.jpg',...
%     '.\KinectCalibrationIMGs\ph32.jpg',...
%     '.\KinectCalibrationIMGs\ph36.jpg',...
%     };
% 
% % Detect checkerboards in images
% [imagePoints, boardSize, imagesUsed] = detectCheckerboardPoints(imageFileNames);
% imageFileNames = imageFileNames(imagesUsed);
% 
% % Read the first image to obtain image size
% originalImage = imread(imageFileNames{1});
% [mrows, ncols, ~] = size(originalImage);
%   
% % Generate world coordinates of the corners of the squares
% squareSize = 18;  % in units of 'millimeters'
% worldPoints = generateCheckerboardPoints(boardSize, squareSize);
% 
% % Calibrate the camera
% [cameraParamsW, imagesUsedW, estimationErrorsW] = estimateCameraParameters(imagePoints, worldPoints, ...
%     'EstimateSkew', true, 'EstimateTangentialDistortion', false, ...
%     'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'millimeters', ...
%     'InitialIntrinsicMatrix', [], 'InitialRadialDistortion', [], ...
%     'ImageSize', [mrows, ncols]);

%estrazione parametri estrinseci(Rs=matrici di rotazione, Ts=matrici di Traslazione, IPts=ImagePoints)
[Rs,Ts,IPts]=ExstrinscsFromVideo('./videos/10_2.mp4',cameraParamsW);

% au = -ku*f (hor scale factor*focal length)
au = cameraParamsW.FocalLength(1);
% av = kv*f (ver scale factor*focal length)
av = cameraParamsW.FocalLength(2);

% principal point coordinates
u0 = cameraParamsW.PrincipalPoint(1);
v0 = cameraParamsW.PrincipalPoint(2);

Intrinsics = [au 0 u0 0;...
             0 av v0 0;...
             0  0  1 0];

Frames=size(Rs,3);
count0=1;

while count0<=Frames
    Extrinsics = [Rs(:,:,count0) Ts(:,:,count0) ; 0 0 0 1];
    T=Intrinsics*Extrinsics;
    dIpts=size(IPts,1);
    count1=1;
    while count1<=dIpts
        A = [IPts(count1,1,count0) IPts(count1,2,count0) 1]';
        %Matrice finale in cui sono presenti le coordinate della maschera
        Ms(count0,count1,:) = T\A;
        a = ((IPts(count1,1,count0)-IPts(1,1,count0))*Rs(3,1,count0))-(au*Rs(1,1,count0));
        b = ((IPts(count1,1,count0)-IPts(1,1,count0))*Rs(3,2,count0))-(au*Rs(1,2,count0));
        c = ((IPts(count1,2,count0)-IPts(1,2,count0))*Rs(3,1,count0))-(av*Rs(2,1,count0));
        d = ((IPts(count1,2,count0)-IPts(1,2,count0))*Rs(3,2,count0))-(av*Rs(2,2,count0));
        MU = [a b ; c d];
        Zi= Ms(count0,count1,4);
        e=(((au*Rs(1,3,count0))-((IPts(count1,1,count0)-IPts(1,1,count0))*Rs(3,3,count0)))*Zi)+(au*Ts(1,1,count0))-((IPts(count1,1,count0)-IPts(1,1,count0))*Ts(3,1,count0));
        f=(((au*Rs(2,3,count0))-((IPts(count1,2,count0)-IPts(1,2,count0))*Rs(3,3,count0)))*Zi)+(av*Ts(3,1,count0))-((IPts(count1,2,count0)-IPts(1,2,count0))*Ts(3,1,count0));
        EF=[e ; f];
        C2D=MU\EF;
        Xs(count0,count1)=C2D(1,1);
        Ys(count0,count1)=C2D(2,1);
        Zs(count0,count1)=Zi;
        %MUi = MU*[Ms(count0,count1,1) Ms(count0,count1,3)]';
        %Zs(count0,count1)=((-1)*((MUi(1)-(au*Rs(1,3,count0)-(au*Ts(1,1,count0))+((IPts(count1,1,count0)-IPts(1,1,count0))*Ts(3,1,count0))))/((IPts(count1,1,count0)-IPts(1,1,count0)*Rs(3,3,count0)))))-10;
        count1=count1+1;
    end
    count0=count0+1;
       
end


X=Ms(:,:,1);
Y=Ms(:,:,3);
Z=Ms(:,:,4);
[mean3D,mean2D,RMSE2,RMSE3]=MeanCBRMSE(X,Y,Z,'./KinectDataSource/data_10_2');
%scatter3(Xs(2,:),Ys(2,:),Zs(2,:));
c=[X(47,:)' (Y(47,:)*(-1))' Z(47,:)'];
plotmatrix(c);
disp(mean3D);
std2D=std(RMSE2);
std3D=std(RMSE3);